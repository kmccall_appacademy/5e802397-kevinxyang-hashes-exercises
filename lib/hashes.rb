# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split(' ')
  hash = Hash.new(0)

  words.each do |word|
    hash[word] = word.length
  end

  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)

  word.chars.each do |letter|
    hash[letter] += 1
  end

  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}

  arr.each do |ele|
    hash[ele] = 0
  end

  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)

  numbers.each do |num|
    hash[:even] += 1 if num.even?
    hash[:odd] += 1 if num.odd?
  end

  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hashy = Hash.new(0)

  string.each_char do |chr|
    hashy[chr] += 1
  end

  hashy.sort_by { |k, v| v }[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  bday_arr = students.select { |name, birth_month| birth_month >= 7 }.keys
  results = []

  idx = 0
  while idx < bday_arr.length - 1
    step = idx + 1
    while step < bday_arr.length
      results << [bday_arr[idx], bday_arr[step]]
      step += 1
    end
    idx += 1
  end

  results
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hashy = Hash.new(0)

  specimens.each do |animal|
    hashy[animal] += 1
  end

  species_count = hashy.sort_by { |animal, count| count }

  number_of_species = specimens.uniq.length
  smallest_population_size = species_count[0][1]
  largest_population_size = species_count[-1][1]

  (number_of_species ** 2) * (smallest_population_size / largest_population_size)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal = normal_sign.delete(",.!").downcase
  vandalized = vandalized_sign.delete(",.!").downcase

  normal_hash = character_count(normal)
  vandalized_hash = character_count(vandalized)

  normal_hash.all? { |k, v| normal_hash[k] >= vandalized_hash[k] }
end

def character_count(str)
  hashy = Hash.new(0)

  str.each_char do |chr|
    next if chr == " "
    hashy[chr] += 1
  end

  hashy
end


## Target completion date: May 11, 2017 - EOD
